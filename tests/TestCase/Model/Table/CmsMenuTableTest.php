<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CmsMenuTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CmsMenuTable Test Case
 */
class CmsMenuTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CmsMenuTable
     */
    public $CmsMenu;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.cms_menu'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CmsMenu') ? [] : ['className' => 'App\Model\Table\CmsMenuTable'];
        $this->CmsMenu = TableRegistry::get('CmsMenu', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CmsMenu);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
