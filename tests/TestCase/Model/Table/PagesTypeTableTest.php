<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PagesTypeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PagesTypeTable Test Case
 */
class PagesTypeTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PagesTypeTable
     */
    public $PagesType;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.pages_type'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PagesType') ? [] : ['className' => 'App\Model\Table\PagesTypeTable'];
        $this->PagesType = TableRegistry::get('PagesType', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PagesType);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
