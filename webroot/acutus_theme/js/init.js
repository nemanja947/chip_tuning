
$(document).ready(function(){

    //Hide loader
    $('.loading').hide();
    // Initialize switcher checkboxes
    var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));

    elems.forEach(function(html) {
        var switchery = new Switchery(html);
    });
});

// Creates cake-like flash message for Genera
var cake = {
    flash:
        function(html, loading) {
            console.log(html);
            $('#authMessage').remove();
            if($('#flashMessage').length) {
                $('#flashMessage').html(html);
            } else {
                $("div.content").prepend($("<div>").attr("id", "flashMessage").attr("class", "message alert alert-info").html(html));
            }
            $('#flashMessage').attr('class', typeof(loading) == 'undefined' ? 'message alert alert-info' : 'message  alert alert-success');
        },

    clearFlash:
        function() {
            $('#flashMessage, #authMessage').remove();
        }
}
