<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'AcutusTheme' => $baseDir . '/plugins/AcutusTheme/',
        'AssetCompress' => $baseDir . '/vendor/markstory/asset_compress/',
        'Attachments' => $baseDir . '/vendor/codekanzlei/cake-attachments/',
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'Burzum/Imagine' => $baseDir . '/vendor/burzum/cakephp-imagine-plugin/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'FrontendBridge' => $baseDir . '/vendor/codekanzlei/cake-frontend-bridge/',
        'Josegonzalez/Upload' => $baseDir . '/vendor/josegonzalez/cakephp-upload/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'MiniAsset' => $baseDir . '/vendor/markstory/mini-asset/'
    ]
];