if(typeof Frontend.AppController != 'function') {
	Frontend.AppController = Frontend.Controller.extend({
		baseComponents: ['Attachments'],
		_initialize: function() {
			this.startup();
		}
	});
}