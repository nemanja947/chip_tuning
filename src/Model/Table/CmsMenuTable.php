<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CmsMenu Model
 *
 * @property \Cake\ORM\Association\BelongsTo $ParentCmsMenu
 * @property \Cake\ORM\Association\HasMany $ChildCmsMenu
 *
 * @method \App\Model\Entity\CmsMenu get($primaryKey, $options = [])
 * @method \App\Model\Entity\CmsMenu newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CmsMenu[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CmsMenu|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CmsMenu patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CmsMenu[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CmsMenu findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 * @mixin \Cake\ORM\Behavior\TreeBehavior
 */
class CmsMenuTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cms_menu');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Tree');

        $this->belongsTo('ParentCmsMenu', [
            'className' => 'CmsMenu',
            'foreignKey' => 'parent_id'
        ]);
        $this->hasMany('ChildCmsMenu', [
            'className' => 'CmsMenu',
            'foreignKey' => 'parent_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

//        $validator
//            ->integer('lft')
//            ->requirePresence('lft', 'create')
//            ->allowEmpty('lft');
//
//        $validator
//            ->integer('rght')
//            ->requirePresence('rght', 'create')
//            ->allowEmpty('rght');

        $validator
            ->requirePresence('controller', 'create')
            ->notEmpty('controller');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->allowEmpty('icon');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['parent_id'], 'ParentCmsMenu'));

        return $rules;
    }
}
