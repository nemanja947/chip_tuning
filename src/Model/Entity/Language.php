<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Language Entity
 *
 * @property int $id
 * @property string $locale
 * @property string $locale_url
 * @property string $title
 * @property string $date_format
 * @property bool $is_active
 * @property bool $is_front
 * @property bool $is_cms
 * @property int $ordering
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $created
 */
class Language extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
