<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Page Entity
 *
 * @property int $id
 * @property int $parent_id
 * @property int $lft
 * @property int $rght
 * @property string $type
 * @property string $anchor
 * @property string $params
 * @property string $title
 * @property string $full_title
 * @property string $content
 * @property string $menu_subtitle
 * @property string $menu_text
 * @property \Cake\I18n\Time $published
 * @property bool $is_active
 * @property bool $new_window
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property bool $is_visible
 *
 * @property \App\Model\Entity\Page $parent_page
 * @property \App\Model\Entity\Page[] $child_pages
 */
class Page extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
