<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property int $id
 * @property string $name
 * @property string $username
 * @property string $password
 * @property int $group_id
 * @property string $email
 * @property string $facebook
 * @property string $twitter
 * @property string $linkedin
 * @property string $google
 * @property string $landing_page
 * @property bool $is_active
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $created
 * @property string $about_me
 * @property $photo
 * @property string $dir
 * @property int $is_fixed_navbar
 * @property int $is_fixed_menu
 * @property int $is_right_to_left
 * @property int $is_menu_right
 * @property string $theme
 *
 * @property \App\Model\Entity\Group $group
 */
class User extends Entity
{


    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */

    protected $_accessible = [
        '*' => true,
        'id' => false,
        'password' => true,
        'attachments' => true,
        'attachment_uploads' => true
    ];



    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }
}
