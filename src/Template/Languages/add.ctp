<div class="page-header no-padding">
    <ul class="breadcrumb" id="page-breadcrumb">
        <li><?php echo $this->Html->link(__('Languages'), array('action' => 'index')); ?></li>
                        <li class="active"><?php echo $this->Html->link(__('Add'), array('action' => 'add')); ?></li>
        
    </ul>
</div>

<?= $this->Flash->render(); ?>
<div class="row">
    <div class="col-sm-12">
        <?= $this->Form->create($language) ?>
        <div class="panel-body">
            <?php
                echo $this->Form->input('locale');
                echo $this->Form->input('locale_url');
                echo $this->Form->input('title');
                echo $this->Attachments->attachmentsArea($language, [
                    'label' => 'File Attachments',
                    'formFieldName' => 'attachment_uploads'
                ]);
                echo $this->Form->input('date_format');
                echo $this->Form->input('is_active', ['type'=>'checkbox', 'hiddenField' => true]);
                echo $this->Form->input('is_front');
                echo $this->Form->input('is_cms', ['type'=>'checkbox', 'hiddenField' => true]);
                echo $this->Form->input('ordering');
            ?>
        </div>
        <div class="panel-footer text-right">
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>

        <div class="panel-heading">
            <span class='panel-title'><?php echo __('Actions'); ?></span>
        </div>
        <div class="panel-body buttons-with-margins"  >
            <?php echo $this->Html->link(__('List Languages'), array('action' => 'index'), array('class' => 'btn btn-primary')) ?>
                        </div>
    </div>
</div>

