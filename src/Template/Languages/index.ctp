<div class="container">
<ul class="breadcrumb" id="page-breadcrumb">
    <li><?= $this->Html->link('Language', ['action' => 'index']); ?></li>
    <li class="active"><?= $this->Html->link('Index', [ 'action' => 'index']); ?></li>
</ul>

<div class="row">
    <div class="col-md-3">
        <h1 class="page-header margin-top-none"><?= __('Languages'); ?></h1>
    </div>
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <span class="pull-right"> <?= $this->Html->link('<i class="menu-icon fa fa-plus"></i> ' . __('New Languages'), ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-primary']); ?></span>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $this->Flash->render(); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table id="data" class="table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                                <th><?= $this->Paginator->sort('id') ?></th>
                                <th><?= $this->Paginator->sort('locale') ?></th>
                                <th><?= $this->Paginator->sort('locale_url') ?></th>
                                <th><?= $this->Paginator->sort('title') ?></th>
                                <th><?= $this->Paginator->sort('date_format') ?></th>
                                <th><?= $this->Paginator->sort('is_active') ?></th>
                                <th><?= $this->Paginator->sort('is_front') ?></th>
                                <th><?= $this->Paginator->sort('is_cms') ?></th>
                                <th><?= $this->Paginator->sort('ordering') ?></th>
                                <th><?= $this->Paginator->sort('modified') ?></th>
                                <th><?= $this->Paginator->sort('created') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($languages as $language): ?>
            <tr>
                                <td><?= $this->Number->format($language->id) ?></td>
                                <td><?= h($language->locale) ?></td>
                                <td><?= h($language->locale_url) ?></td>
                                <td><?= h($language->title) ?></td>
                                <td><?= h($language->date_format) ?></td>
                                <td><?= $this->Format->toggle($language, 'is_active') ?></td>
                                <td><?= $this->Format->toggle($language, 'is_front') ?></td>
                                <td><?= $this->Format->toggle($language, 'is_cms') ?></td>
                                <td><?= $this->Number->format($language->ordering) ?></td>
                                <td><?= h($language->modified) ?></td>
                                <td><?= h($language->created) ?></td>
                                <td class="actions">
                    <?= $this->Html->link('<i class="menu-icon fa  fa-pencil"></i>', ['action' => 'edit', $language->id],['title' => __('Edit', true), 'escape' => false, 'class' => 'btn btn-primary']) ?>
                        <?= $this->Form->postLink('<i class="menu-icon fa fa-trash-o"></i>', ['action' => 'delete', $language->id], ['class' => 'btn btn-danger', 'title' => __('Delete', true), 'escape' => false,'confirm' => __('Are you sure you want to delete # {0}?',$language->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
    </ul>
    <p><?= $this->Paginator->counter() ?></p>
</div>
</div>
