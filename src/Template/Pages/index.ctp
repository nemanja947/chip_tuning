<?php use Cake\Routing\Router; ?>
<div class="container">
<ul class="breadcrumb" id="page-breadcrumb">
    <li><?= $this->Html->link('Page', ['action' => 'index']); ?></li>
    <li class="active"><?= $this->Html->link('Index', [ 'action' => 'index']); ?></li>
</ul>

<div class="row">
    <div class="col-md-3">
        <h1 class="page-header margin-top-none"><?= __('Pages'); ?></h1>
    </div>
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <span class="pull-right"> <?= $this->Html->link('<i class="menu-icon fa fa-plus"></i> ' . __('New Pages'), ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-primary']); ?></span>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $this->Flash->render(); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel widget-tasks panel-dark-gray" >
            <div class="panel-heading">
                <span class="panel-title"><i class="panel-title-icon fa fa-tasks"></i>Tree</span>
            </div> <!-- / .panel-heading --><!-- Without vertical padding -->
            <div class="panel-body no-padding-vr" >
                <div class="task sidebar-menu">
                    <?php echo $this->Content->MyTree($menu) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <table id="data" class="table table-hover table-bordered" cellspacing="0" width="100%">
            <thead>
            <tr>
                                <th><?= $this->Paginator->sort('id') ?></th>
                                <th><?= $this->Paginator->sort('title') ?></th>
                                <th><?= $this->Paginator->sort('menu_subtitle') ?></th>
                                <th><?= $this->Paginator->sort('is_active') ?></th>
                                <th><?= $this->Paginator->sort('is_visible') ?></th>
                                <th><?= $this->Paginator->sort('new_window') ?></th>
                                <th><?= $this->Paginator->sort('modified') ?></th>
                                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($pages as $page): ?>
            <tr>
                                <td><?= $this->Number->format($page->id) ?></td>
                                <td><?= h($page->title) ?></td>
                                <td><?= h($page->menu_subtitle) ?></td>
                                <td><?= $this->Format->toggle($page, 'is_active') ?></td>
                                <td><?= $this->Format->toggle($page, 'is_visible') ?></td>
                                <td><?= $this->Format->toggle($page, 'new_window') ?></td>
                                <td><?= h($page->modified) ?></td>
                                <td class="actions">
                    <?= $this->Html->link('<i class="menu-icon fa  fa-pencil"></i>', ['action' => 'edit', $page->id],['title' => __('Edit', true), 'escape' => false, 'class' => 'btn btn-primary']) ?>
                        <?= $this->Form->postLink('<i class="menu-icon fa fa-trash-o"></i>', ['action' => 'delete', $page->id], ['class' => 'btn btn-danger', 'title' => __('Delete', true), 'escape' => false,'confirm' => __('Are you sure you want to delete # {0}?',$page->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
</div>
<script type="application/javascript">
    $().ready(function(){

        $('ol.sortable').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div',
            helper:	'clone',
            items: 'li',
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 4,
            isTree: true,
            expandOnHover: 1,
            excludeRoot:true,
            startCollapsed: false,
            change: function(){
                //console.log('Relocated item');
            },
            relocate: function () {
                //$('.loading').show();
                arraied = $('ol.sortable').nestedSortable('toArray');
                var jsonData=JSON.stringify(arraied);

                var url="<?php echo Router::url(array('controller'=>'Pages','action'=>'reorganize', '_ext' => 'json'));?>";
                $.ajax({
                    url: url,
                    type: 'post',
                    data:jsonData,
                    contentType : 'application/json; charset=utf-8',
                    success: function (response) {
                    },
                    error: function(xhr, error){
                        console.log(error);
                    }
                });
            }
        });

        $('.disclose').on('click', function() {
            $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
            $(this).toggleClass('fa fa-plus').toggleClass('fa fa-minus');
        });

        $('.change-title').on('click', function () {
            var menu_item = $(this);
            var menu_id = menu_item.data('id');
            var url="<?php echo Router::url(array('controller'=>'Pages','action'=>'changeTitle', '_ext' => 'json'));?>";
            swal({
                title: "Menu title!",
                text: "Change menu title:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Title"
            }, function (inputValue) {
                if (inputValue === false) return false;
                else if (inputValue === "") {
                    swal.showInputError("Title can't be empty!");
                    return false
                }else {
                    swal("Nice!", "You change title to: " + inputValue, "success");
                    var data = {'id':menu_id,'title':inputValue};
                    $.ajax({
                        url: url,
                        type: 'post',
                        data: JSON.stringify(data),
                        contentType : 'application/json; charset=utf-8',
                        success: function (response) {
                            menu_item.text(inputValue);
                        },
                        error: function(xhr, error){
                            console.log(error);
                        }
                    });
                }
            });
        });
//        $('.proba').click(function (e) {
//            swal("Oops!", "Something went wrong on the page!", "error");
//        });
    });

</script>