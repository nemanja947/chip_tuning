<div class="page-header no-padding">
    <ul class="breadcrumb" id="page-breadcrumb">
        <li><?php echo $this->Html->link(__('Pages'), array('action' => 'index')); ?></li>
                        <li class="active"><?php echo $this->Html->link(__('Add'), array('action' => 'add')); ?></li>
        
    </ul>
</div>

<?= $this->Flash->render(); ?>
<div class="row">
    <div class="col-sm-12">
        <?= $this->Form->create($page) ?>
        <div class="panel-body">
            <?php
                    echo $this->Form->input('parent_id', ['options' => $parentPages, 'empty' => true]);
                    echo $this->Form->input('type_id', ['options' => $types, 'empty' => false]);
            ?>
            <div class="link">
            <?php
                    echo $this->Form->input('anchor', ['class' => 'form-control link']);
            ?>
            </div>
            <div class="dinamic">
            <?php
                    echo $this->Form->input('service', ['options' => $resources]);
                    echo $this->Form->input('params');
            ?>
            </div>
            <?php
                    echo $this->Form->input('title');
                    echo $this->Form->input('full_title');
                    echo $this->Form->input('content', ['type'=>'textarea' , 'class' => 'rte']);
                    echo $this->Form->input('menu_subtitle');
                    echo $this->Form->input('menu_text');
                    echo $this->Form->input('is_active', ['type'=>'checkbox', 'hiddenField' => true]);
                    echo $this->Form->input('new_window', ['type'=>'checkbox', 'hiddenField' => true]);
                    echo $this->Form->input('is_visible', ['type'=>'checkbox', 'hiddenField' => true]);
            ?>
        </div>
        <div class="panel-footer text-right">
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>

        <div class="panel-heading">
            <span class='panel-title'><?php echo __('Actions'); ?></span>
        </div>
        <div class="panel-body buttons-with-margins"  >
            <?php echo $this->Html->link(__('List Pages'), array('action' => 'index'), array('class' => 'btn btn-primary')) ?>
        </div>
    </div>
</div>

<script type="application/javascript">
    $().ready(function () {
        $('.link').hide();
        $('.dinamic').hide();

        $('#type-id').change(function () {
            var value = $(this).val();
            if( value == 2 ) {
                $('.link').hide();
                $('.dinamic').fadeIn('slow');
            }
            else if( value == 3 ) {
                $('.dinamic').hide();
                $('.link').fadeIn('slow');
            }
            else{
                $('.link').hide();
                $('.dinamic').hide();
            }
        });
    });
</script>

