<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Page'), ['action' => 'edit', $page->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Page'), ['action' => 'delete', $page->id], ['confirm' => __('Are you sure you want to delete # {0}?', $page->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Pages'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Page'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Parent Pages'), ['controller' => 'Pages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Parent Page'), ['controller' => 'Pages', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="pages view large-9 medium-8 columns content">
    <h3><?= h($page->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Parent Page') ?></th>
            <td><?= $page->has('parent_page') ? $this->Html->link($page->parent_page->id, ['controller' => 'Pages', 'action' => 'view', $page->parent_page->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Anchor') ?></th>
            <td><?= h($page->anchor) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($page->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Full Title') ?></th>
            <td><?= h($page->full_title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Menu Subtitle') ?></th>
            <td><?= h($page->menu_subtitle) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Menu Text') ?></th>
            <td><?= h($page->menu_text) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($page->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lft') ?></th>
            <td><?= $this->Number->format($page->lft) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rght') ?></th>
            <td><?= $this->Number->format($page->rght) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Published') ?></th>
            <td><?= h($page->published) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($page->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($page->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Active') ?></th>
            <td><?= $page->is_active ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('New Window') ?></th>
            <td><?= $page->new_window ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Visible') ?></th>
            <td><?= $page->is_visible ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Type') ?></h4>
        <?= $this->Text->autoParagraph(h($page->type)); ?>
    </div>
    <div class="row">
        <h4><?= __('Params') ?></h4>
        <?= $this->Text->autoParagraph(h($page->params)); ?>
    </div>
    <div class="row">
        <h4><?= __('Content') ?></h4>
        <?= $this->Text->autoParagraph(h($page->content)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Pages') ?></h4>
        <?php if (!empty($page->child_pages)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Parent Id') ?></th>
                <th scope="col"><?= __('Lft') ?></th>
                <th scope="col"><?= __('Rght') ?></th>
                <th scope="col"><?= __('Type') ?></th>
                <th scope="col"><?= __('Anchor') ?></th>
                <th scope="col"><?= __('Params') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Full Title') ?></th>
                <th scope="col"><?= __('Content') ?></th>
                <th scope="col"><?= __('Menu Subtitle') ?></th>
                <th scope="col"><?= __('Menu Text') ?></th>
                <th scope="col"><?= __('Published') ?></th>
                <th scope="col"><?= __('Is Active') ?></th>
                <th scope="col"><?= __('New Window') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Is Visible') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($page->child_pages as $childPages): ?>
            <tr>
                <td><?= h($childPages->id) ?></td>
                <td><?= h($childPages->parent_id) ?></td>
                <td><?= h($childPages->lft) ?></td>
                <td><?= h($childPages->rght) ?></td>
                <td><?= h($childPages->type) ?></td>
                <td><?= h($childPages->anchor) ?></td>
                <td><?= h($childPages->params) ?></td>
                <td><?= h($childPages->title) ?></td>
                <td><?= h($childPages->full_title) ?></td>
                <td><?= h($childPages->content) ?></td>
                <td><?= h($childPages->menu_subtitle) ?></td>
                <td><?= h($childPages->menu_text) ?></td>
                <td><?= h($childPages->published) ?></td>
                <td><?= h($childPages->is_active) ?></td>
                <td><?= h($childPages->new_window) ?></td>
                <td><?= h($childPages->created) ?></td>
                <td><?= h($childPages->modified) ?></td>
                <td><?= h($childPages->is_visible) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Pages', 'action' => 'view', $childPages->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Pages', 'action' => 'edit', $childPages->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Pages', 'action' => 'delete', $childPages->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childPages->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
