<div class="page-header no-padding">
    <ul class="breadcrumb" id="page-breadcrumb">
        <li><?php echo $this->Html->link(__('Pages'), array('action' => 'index')); ?></li>
                <li class="active"><?php echo $this->Html->link(__('Edit'), array('action' => 'edit')); ?></li>
                
    </ul>
</div>

<?= $this->Flash->render(); ?>
<div class="row">
    <div class="col-sm-12">
        <?= $this->Form->create($page) ?>
        <div class="panel-body">
            <?php
                echo $this->Form->input('parent_id', ['options' => $parentPages, 'empty' => true]);
                echo $this->Form->input('type_id', ['options' => $types, 'empty' => false]);
            ?>
            <div class="link">
                <?php
                echo $this->Form->input('anchor', ['class' => 'form-control link']);
                ?>
            </div>
            <div class="dinamic">
                <?php
                echo $this->Form->input('service', ['options' => $resources]);
                echo $this->Form->input('params');
                ?>
            </div>
            <?php
                echo $this->Form->input('title');
                echo $this->Form->input('full_title');
                echo $this->Form->input('content', ['type'=>'textarea' , 'class' => 'rte']);
                echo $this->Form->input('menu_subtitle');
                echo $this->Form->input('menu_text');
                echo $this->Form->input('is_active', ['type'=>'checkbox', 'hiddenField' => true]);
                echo $this->Form->input('new_window', ['type'=>'checkbox', 'hiddenField' => true]);
                echo $this->Form->input('is_visible', ['type'=>'checkbox', 'hiddenField' => true]);
                ?>
        </div>
        <div class="panel-footer text-right">
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>

        <div class="panel-heading">
            <span class='panel-title'><?php echo __('Actions'); ?></span>
        </div>
        <div class="panel-body buttons-with-margins"  >
            <?php echo $this->Html->link(__('List Pages'), array('action' => 'index'), array('class' => 'btn btn-primary')) ?>
                            <?= $this->Form->postLink(
            __('Delete'),
            ['action' => 'delete', $page->id],
            [
            'confirm' => __('Are you sure you want to delete # {0}?', $page->id),
            'class' => 'btn btn-danger'
            ])
            ?>
        </div>
    </div>
</div>

<script type="application/javascript">
    $().ready(function () {

        var type_list =  $('#type-id');
        var type = type_list.val();

        if( type == 2)
            $('.link').hide();
        else if(type == 3)
            $('.dinamic').hide();
        else{
            $('.link').hide();
            $('.dinamic').hide();
        }

        type_list.change(function () {
            var value = $(this).val();
            if( value == 2 ) {
                $('.link').hide();
                $('.dinamic').fadeIn('slow');
            }
            else if( value == 3 ) {
                $('.dinamic').hide();
                $('.link').fadeIn('slow');
            }
            else{
                $('.link').hide();
                $('.dinamic').hide();
            }
        });
    });
</script>

