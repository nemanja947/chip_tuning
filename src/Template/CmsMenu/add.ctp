<div class="page-header no-padding">
    <ul class="breadcrumb" id="page-breadcrumb">
        <li><?php echo $this->Html->link(__('Cms Menu'), array('action' => 'index')); ?></li>
        <li class="active"><?php echo $this->Html->link(__('Add'), array('action' => 'add')); ?></li>

    </ul>
</div>

<?= $this->Flash->render(); ?>
<div class="row">
    <div class="col-sm-12">
    <?= $this->Form->create($cmsMenu) ?>
        <div class="panel-body">
        <?php
            echo $this->Form->input('parent_id', ['options' => $parentCmsMenu, 'empty' => true]);
//            echo $this->Form->input('controller');
            echo $this->Form->input('controller', ['options' => $resources]);
            echo $this->Form->input('icon',['class'=>'form-control icp icp-auto iconpicker-element iconpicker-input']);
            echo $this->Form->input('title');
        ?>
        </div>
        <div class="panel-footer text-right">
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>

        <div class="panel-heading">
            <span class='panel-title'><?php echo __('Actions'); ?></span>
        </div>
        <div class="panel-body buttons-with-margins"  >
            <?php echo $this->Html->link(__('List Cms Menu'), array('action' => 'index'), array('class' => 'btn btn-primary')) ?>
                    </div>
    </div>
</div>



