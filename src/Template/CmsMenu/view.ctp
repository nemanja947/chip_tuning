<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Cms Menu'), ['action' => 'edit', $cmsMenu->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cms Menu'), ['action' => 'delete', $cmsMenu->id], ['confirm' => __('Are you sure you want to delete # {0}?', $cmsMenu->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cms Menu'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cms Menu'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Parent Cms Menu'), ['controller' => 'CmsMenu', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Parent Cms Menu'), ['controller' => 'CmsMenu', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cmsMenu view large-9 medium-8 columns content">
    <h3><?= h($cmsMenu->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Parent Cms Menu') ?></th>
            <td><?= $cmsMenu->has('parent_cms_menu') ? $this->Html->link($cmsMenu->parent_cms_menu->id, ['controller' => 'CmsMenu', 'action' => 'view', $cmsMenu->parent_cms_menu->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Controller') ?></th>
            <td><?= h($cmsMenu->controller) ?></td>
        </tr>
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($cmsMenu->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Icon') ?></th>
            <td><?= h($cmsMenu->icon) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($cmsMenu->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Lft') ?></th>
            <td><?= $this->Number->format($cmsMenu->lft) ?></td>
        </tr>
        <tr>
            <th><?= __('Rght') ?></th>
            <td><?= $this->Number->format($cmsMenu->rght) ?></td>
        </tr>
        <tr>
            <th><?= __('Created') ?></th>
            <td><?= h($cmsMenu->created) ?></td>
        </tr>
        <tr>
            <th><?= __('Modified') ?></th>
            <td><?= h($cmsMenu->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Cms Menu') ?></h4>
        <?php if (!empty($cmsMenu->child_cms_menu)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Parent Id') ?></th>
                <th><?= __('Lft') ?></th>
                <th><?= __('Rght') ?></th>
                <th><?= __('Controller') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Icon') ?></th>
                <th><?= __('Created') ?></th>
                <th><?= __('Modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($cmsMenu->child_cms_menu as $childCmsMenu): ?>
            <tr>
                <td><?= h($childCmsMenu->id) ?></td>
                <td><?= h($childCmsMenu->parent_id) ?></td>
                <td><?= h($childCmsMenu->lft) ?></td>
                <td><?= h($childCmsMenu->rght) ?></td>
                <td><?= h($childCmsMenu->controller) ?></td>
                <td><?= h($childCmsMenu->title) ?></td>
                <td><?= h($childCmsMenu->icon) ?></td>
                <td><?= h($childCmsMenu->created) ?></td>
                <td><?= h($childCmsMenu->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'CmsMenu', 'action' => 'view', $childCmsMenu->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'CmsMenu', 'action' => 'edit', $childCmsMenu->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'CmsMenu', 'action' => 'delete', $childCmsMenu->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childCmsMenu->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
