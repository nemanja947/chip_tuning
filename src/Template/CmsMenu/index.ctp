<?php use Cake\Routing\Router; ?>
<ul class="breadcrumb" id="page-breadcrumb">
    <li><?= $this->Html->link('Cms Menu', ['action' => 'index']); ?></li>
    <li class="active"><?= $this->Html->link('Index', [ 'action' => 'index']); ?></li>
</ul>

<div class="row">
    <div class="col-md-3">
        <h1 class="page-header margin-top-none"><?= __('Cms Menu'); ?></h1>
    </div>
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <span class="pull-right"> <?= $this->Html->link('<i class="menu-icon fa fa-plus"></i> ' . __('New Cms Menu'), ['action' => 'add'], ['escape' => false, 'class' => 'proba btn btn-primary' , 'onclick']); ?></span>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $this->Flash->render(); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel widget-tasks panel-dark-gray" >
            <div class="panel-heading">
                <span class="panel-title"><i class="panel-title-icon fa fa-tasks"></i>Tree</span>
            </div> <!-- / .panel-heading --><!-- Without vertical padding -->
            <div class="panel-body no-padding-vr" >
                <div class="task sidebar-menu">
                    <?php echo $this->Content->MyTree($menu) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <table id="data" class="table table-hover table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('parent_id') ?></th>
                <th><?= $this->Paginator->sort('controller') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('icon') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($cmsMenu as $cmsMenu): ?>
            <tr>
                <td><?= $this->Number->format($cmsMenu->id) ?></td>
                <td><?= $cmsMenu->has('parent_cms_menu') ? $this->Html->link($cmsMenu->parent_cms_menu->id, ['controller' => 'CmsMenu', 'action' => 'view', $cmsMenu->parent_cms_menu->id]) : '' ?></td>
                <td><?= h($cmsMenu->controller) ?></td>
                <td><?= h($cmsMenu->title) ?></td>
                <td><?= h($cmsMenu->icon) ?></td>
                <td><?= h($cmsMenu->created) ?></td>
                <td><?= h($cmsMenu->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="menu-icon fa  fa-pencil"></i>', ['action' => 'edit', $cmsMenu->id],['title' => __('Edit', true), 'escape' => false, 'class' => 'btn btn-primary']) ?>
                        <?= $this->Form->postLink('<i class="menu-icon fa fa-trash-o"></i>', ['action' => 'delete', $cmsMenu->id], ['class' => 'btn btn-danger', 'title' => __('Delete', true), 'escape' => false,'confirm' => __('Are you sure you want to delete # {0}?',$cmsMenu->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
</div>

<script type="application/javascript">
    $().ready(function(){



        $('ol.sortable').nestedSortable({
            forcePlaceholderSize: true,
            handle: 'div',
            helper:	'clone',
            items: 'li',
            opacity: .6,
            placeholder: 'placeholder',
            revert: 250,
            tabSize: 25,
            tolerance: 'pointer',
            toleranceElement: '> div',
            maxLevels: 4,
            isTree: true,
            expandOnHover: 1,
            excludeRoot:true,
            startCollapsed: false,
            change: function(){
                //console.log('Relocated item');
            },
            relocate: function () {
                console.log( $('.loading'));
                $('.loading').show();
                arraied = $('ol.sortable').nestedSortable('toArray');
                var jsonData=JSON.stringify(arraied);

                var url="<?php echo Router::url(array('controller'=>'CmsMenu','action'=>'reorganize', '_ext' => 'json'));?>";
                $.ajax({
                    url: url,
                    type: 'post',
                    data:jsonData,
                    contentType : 'application/json; charset=utf-8',
                    success: function (response) {
                        $('li.current-parent').remove();
                        $('#side-menu li:first').after(response.data.html);

                        //Setovanje menia
                        $('#side-menu').metisMenu();
                        var url = window.location;
                        var element = $('ul.nav a').filter(function() {
                            return this.href == url || url.href.indexOf(this.href) == 0;
                        }).addClass('active').parent().parent().addClass('in').parent();
                        if (element.is('li')) {
                            element.addClass('active');
                        }
                        $('.loading').hide();
                    },
                    error: function(xhr, error){
                        console.log(error);
                    }
                });
            }
        });

        $('.disclose').on('click', function() {
            $(this).closest('li').toggleClass('mjs-nestedSortable-collapsed').toggleClass('mjs-nestedSortable-expanded');
            $(this).toggleClass('fa fa-plus').toggleClass('fa fa-minus');
        });

        $('.change-title').on('click', function () {
            var menu_item = $(this);
            var menu_id = menu_item.data('id');
            var url="<?php echo Router::url(array('controller'=>'CmsMenu','action'=>'changeTitle', '_ext' => 'json'));?>";
            swal({
                title: "Menu title!",
                text: "Change menu title:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Title"
            }, function (inputValue) {
                if (inputValue === false) return false;
                else if (inputValue === "") {
                    swal.showInputError("Title can't be empty!");
                    return false
                }else {
                    swal("Nice!", "You change title to: " + inputValue, "success");
                    var data = {'id':menu_id,'title':inputValue};
                    $.ajax({
                        url: url,
                        type: 'post',
                        data: JSON.stringify(data),
                        contentType : 'application/json; charset=utf-8',
                        success: function (response) {
                            menu_item.text(inputValue);
                            //Refresh side bar menu
                            $('li.current-parent').remove();
                            $('#side-menu li:first').after(response.data.html);

                            //Setovanje menia
                            $('#side-menu').metisMenu();
                            var url = window.location;
                            var element = $('ul.nav a').filter(function() {
                                return this.href == url || url.href.indexOf(this.href) == 0;
                            }).addClass('active').parent().parent().addClass('in').parent();
                            if (element.is('li')) {
                                element.addClass('active');
                            }
                        },
                        error: function(xhr, error){
                            console.log(error);
                        }
                    });
                }
            });
        });
//        $('.proba').click(function (e) {
//            swal("Oops!", "Something went wrong on the page!", "error");
//        });

        // Za brisanje
//        $('.proba').click(function (e) {
//            swal({
//                title: "Are you sure?",
//                text: "You will not be able to recover this imaginary file!",
//                type: "warning",
//                showCancelButton: true,
//                confirmButtonColor: "#DD6B55",
//                confirmButtonText: "Yes, delete it!",
//                cancelButtonText: "No, cancel plx!",
//                closeOnConfirm: false,
//                closeOnCancel: false
//            }, function (isConfirm) {
//                if (isConfirm) {
//                    swal("Deleted!", "Your imaginary file has been deleted.", "success");
//                } else {
//                    swal("Cancelled", "Your imaginary file is safe :)", "error");
//                }
//            });
//        });


    });

</script>

