<div class="page-header no-padding">
    <ul class="breadcrumb" id="page-breadcrumb">
        <li><?php echo $this->Html->link(__('Groups'), array('action' => 'index')); ?></li>
        <li class="active"><?php echo $this->Html->link(__('Edit'), array('action' => 'edit')); ?></li>

    </ul>
</div>

<?= $this->Flash->render(); ?>
<div class="row">
    <div class="col-sm-12">
    <?= $this->Form->create($group) ?>
        <div class="panel-body">
        <?php
            #Create tabs for translations
            echo $this->Content->translations($group->source(), $cms_languages);
            echo $this->Form->input('landing_page');
            echo $this->Form->input('ordering');
            echo $this->Form->input('published', ['type'=>'text','class'=>'form-control datepicker']);
            echo $this->Form->input('text');
        ?>
        </div>
        <div class="panel-footer text-right">
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>

        <div class="panel-heading">
            <span class='panel-title'><?php echo __('Actions'); ?></span>
        </div>
        <div class="panel-body buttons-with-margins"  >
            <?php echo $this->Html->link(__('List Groups'), array('action' => 'index'), array('class' => 'btn btn-primary')) ?>
                        <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $group->id],
                [
                'confirm' => __('Are you sure you want to delete # {0}?', $group->id),
                'class' => 'btn btn-danger'
                ])
                ?>
                    </div>
    </div>
</div>

