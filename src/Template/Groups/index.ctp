
<ul class="breadcrumb" id="page-breadcrumb">
    <li><?= $this->Html->link('Group', ['action' => 'index']); ?></li>
    <li class="active"><?= $this->Html->link('Index', [ 'action' => 'index']); ?></li>
</ul>

<div class="row">
    <div class="col-md-3">
        <h1 class="page-header margin-top-none"><?= __('Groups'); ?></h1>
    </div>
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <span class="pull-right"> <?= $this->Html->link('<i class="menu-icon fa fa-plus"></i> ' . __('New Groups'), ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-primary']); ?></span>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $this->Flash->render(); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table id="data" class="table table-hover table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('landing_page') ?></th>
                <th><?= $this->Paginator->sort('ordering') ?></th>
                <th><?= $this->Paginator->sort('published') ?></th>
                <th><?= $this->Paginator->sort('publish') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($groups as $group): ?>
            <tr>
                <td><?= $this->Number->format($group->id) ?></td>
                <td><?= h($group->name) ?></td>
                <td><?= h($group->landing_page) ?></td>
                <td><?= $this->Number->format($group->ordering) ?></td>
                <td><?= h($group->published) ?></td>
                <td><?= $this->Format->toggle($group, 'is_publish') ?></td>
                <td><?= h($group->modified) ?></td>
                <td><?= h($group->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="menu-icon fa  fa-pencil"></i>', ['action' => 'edit', $group->id],['title' => __('Edit', true), 'escape' => false, 'class' => 'btn btn-primary']) ?>
                        <?= $this->Form->postLink('<i class="menu-icon fa fa-trash-o"></i>', ['action' => 'delete', $group->id], ['class' => 'btn btn-danger', 'title' => __('Delete', true), 'escape' => false,'confirm' => __('Are you sure you want to delete # {0}?',$group->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
</div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
