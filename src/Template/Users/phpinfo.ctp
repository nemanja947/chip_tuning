<div class="col-md-1"></div>
<div class="col-md-10">
    <h2><span><?php echo __('PHP info',true) ?></span></h2>
    <div class="white">
        <iframe src="<?= $this->Url->build(['controller' => 'users', 'action' => 'phpinfo', 1]) ?>" style="overflow-x:hidden;border:none;width:100%" onload="resizeIframe(this)"></iframe>
    </div>
</div>

<script language="javascript" type="text/javascript">
    function resizeIframe(obj) {
        obj.style.height = '550px';
    }
</script>