<div class="page-header no-padding">
    <ul class="breadcrumb" id="page-breadcrumb">
        <li><?php echo $this->Html->link(__('Users'), array('action' => 'index')); ?></li>
        <li class="active"><?php echo $this->Html->link(__('Edit'), array('action' => 'edit')); ?></li>

    </ul>
</div>

<?= $this->Flash->render(); ?>
<div class="row">
    <div class="col-sm-12">
        <?= $this->Form->create($user,['type' => 'file']) ?>
        <div class="panel-body">
            <?php
            echo $this->Form->input('name');
            echo $this->Form->input('username');
            echo $this->Form->input('password');
            echo $this->Form->input('group_id', ['options' => $groups]);
            echo $this->Form->input('email');
            echo $this->Form->input('facebook');
            echo $this->Form->input('twitter');
            echo $this->Form->input('linkedin');
            echo $this->Form->input('google');
            echo $this->Form->input('landing_page');
            echo $this->Form->input('is_active', ['hiddenField' => true]);
            echo $this->Form->input('about_me',['class'=>'rte']);
           // echo $this->Form->input('photo', ['type' => 'file']);
            echo $this->Attachments->attachmentsArea($user, [
                'label' => 'File Attachments',
                'formFieldName' => 'attachment_uploads'
            ]);
            ?>
        </div>
        <div class="panel-footer text-right">
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>

        <div class="panel-heading">
            <span class='panel-title'><?php echo __('Actions'); ?></span>
        </div>
        <div class="panel-body buttons-with-margins"  >
            <?php echo $this->Html->link(__('List Users'), array('action' => 'index'), array('class' => 'btn btn-primary')) ?>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $user->id],
                [
                    'confirm' => __('Are you sure you want to delete # {0}?', $user->id),
                    'class' => 'btn btn-danger'
                ])
            ?>
        </div>
    </div>
</div>

