<div id="page-wrapper" class="background-page">
    <div class="container-fluid">
        <a href="/" class="current-parent btn btn-default" role="button"><span class="fa fa-fw fa-long-arrow-left" aria-hidden="true"> </span> Back</a>
        <div class="row">
            <div class="col-md-4 col-md-offset-4" id="login">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading frame logo-center-form" id="container">
                        <?= $this->Html->image('acutus_logo.png',['class' => 'center-logo','alt' => 'Logo']); ?>
                    </div>
                    <div class="panel-body">
                        <h3 class="text-center">Login</h3>
                        <br>
                        <?= $this->Form->create('null',['templates' => [
                            'formStart' => '<form {{attrs}}>',
                        ],
                        ]); ?>
                            <?= $this->Form->input('username',[
                                'templates' => [
                                    'formGroup' => '{{input}} ',
                                ],
                                'class'=>'form-control input-lg','placeholder'=>'Enter a Username','label'=>false]);?>
                            <?= $this->Form->input('password',[
                                'templates' => [
                                    'formGroup' => '{{input}} ',
                                ],
                                'class'=>'form-control input-lg','placeholder'=>'Your passowrd...','label'=>false]); ?>
                        <div class="col-md-12">
                            <?= $this->Flash->render(); ?>
                        </div>
                        <input type="submit" class="current-parent btn btn-block btn-lg btn-success" value="Login" />
                        <?= $this->Form->end(); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-md-offset-4">
                <p class="text-muted small text-center"><strong>Acutus1.1 </strong> Designed and Created by <a href="http://www.webkom.co/">Nemanja Bjelic</a> <br><span class="text-muted">© 2016. Made by <i class="fa fa-fw fa-heart text-danger"></i> Kielce, Poland.</span></p>
            </div>
        </div>
</div>

<script type="application/javascript">
    $().ready(function () {

    });
</script>