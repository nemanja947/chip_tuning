<?php
    # Form template
    $myTemplates = [
        'button' => '<button class="btn btn-sm btn-primary" {{attrs}}>{{text}}</button>',
        'formStart' => '<form class="form-horizontal"{{attrs}}>',
        'formGroup' => '{{label}}<div class="col-lg-9">{{input}}</div>',
        'label' => '<label class="col-lg-3 control-label" {{attrs}}>{{text}}</label>',
        'nestingLabel' => '<label class="col-sm-3 control-label" {{attrs}}>{{text}}</label><div class="col-sm-8">{{input}}</div>',
    ];
    $this->Form->templates($myTemplates);
?>

<div class="container">
    <h1 class="page-header">Edit Profile</h1>
    <?= $this->Form->create($user,['type' => 'file']) ?>
    <div class="row">
        <!-- left column -->
        <div class="col-md-4 col-sm-6 col-xs-12">
            <div class="text-center">
                <?php
                $attachment = $this->request->session()->read('Auth.User.attachments')[0];
                echo $this->Format->image((object)$attachment, 400, 400,[ 'class' => 'avatar img-circle img-thumbnail','alt' => 'Avatar']);
                ?>
                <h6>Upload a different photo...</h6>
                <?php
                    echo $this->Attachments->attachmentsArea($user, [
                        'label' => false,
                        'formFieldName' => 'attachment_uploads',
                    ]);
                ?>
            </div>
        </div>
        <!-- edit form column -->
        <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
                <?= $this->Flash->render(); ?>
            <h3>Personal info</h3>
                <?php
                    echo $this->Form->input('name');
                    echo $this->Form->input('email');
                    echo $this->Form->input('facebook');
                    echo $this->Form->input('twitter');
                    echo $this->Form->input('linkedin');
                    echo $this->Form->input('google');
                    echo $this->Form->input('username');
                    echo $this->Form->input('old_password', ['type' => 'password', 'required' => false]);
                    echo $this->Form->input('password',['value' => '', 'label' => 'New Password' , 'required' => false]);
                    echo $this->Form->input('confirm_password',['type' => 'password',  'required' => false]);
                ?>
                <div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-8">
                        <?= $this->Form->button(__('Save Changes')) ?>
                    </div>
                </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>
