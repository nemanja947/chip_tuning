
<ul class="breadcrumb" id="page-breadcrumb">
    <li><?= $this->Html->link('User', ['action' => 'index']); ?></li>
    <li class="active"><?= $this->Html->link('Index', [ 'action' => 'index']); ?></li>
</ul>

<div class="row">
    <div class="col-md-3">
        <h1 class="page-header margin-top-none"><?= __('Users'); ?></h1>
    </div>
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <span class="pull-right"> <?= $this->Html->link('<i class="menu-icon fa fa-plus"></i> ' . __('New Users'), ['action' => 'add'], ['escape' => false, 'class' => 'btn btn-primary']); ?></span>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?= $this->Flash->render(); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table id="data" class="table table-hover table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('username') ?></th>
                <th><?= $this->Paginator->sort('group_id') ?></th>
                <th><?= $this->Paginator->sort('email') ?></th>
                <th><?= $this->Paginator->sort('is_active') ?></th>
                <th><?= $this->Paginator->sort('modified') ?></th>
                <th><?= $this->Paginator->sort('created') ?></th>
                <th><?= $this->Paginator->sort('theme') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($users as $user): ?>
            <tr>
                <td><?= $this->Number->format($user->id) ?></td>
                <td><?= h($user->name) ?></td>
                <td><?= h($user->username) ?></td>
                <td><?= $user->has('group') ? $this->Html->link($user->group->name, ['controller' => 'Groups', 'action' => 'view', $user->group->id]) : '' ?></td>
                <td><?= h($user->email) ?></td>
                <td><?= $this->Format->toggle($user, 'is_active') ?></td>
                <td><?= h($user->modified) ?></td>
                <td><?= h($user->created) ?></td>
                <td><?= h($user->theme) ?></td>
                <td class="actions">
                    <?= $this->Html->link('<i class="menu-icon fa  fa-pencil"></i>', ['action' => 'edit', $user->id],['title' => __('Edit', true), 'escape' => false, 'class' => 'btn btn-primary']) ?>
                        <?= $this->Form->postLink('<i class="menu-icon fa fa-trash-o"></i>', ['action' => 'delete', $user->id], ['class' => 'btn btn-danger', 'title' => __('Delete', true), 'escape' => false,'confirm' => __('Are you sure you want to delete # {0}?',$user->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    </div>
</div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
