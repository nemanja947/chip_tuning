<div class="sidebar_inner">
    <div class="profile-sidebar">
        <!-- SIDEBAR USER TITLE -->
        <div class="profile-usertitle">
            <div class="profile-header-img">
                <?php
                $attachment = $this->request->session()->read('Auth.User.attachments')[0];
                echo $this->Format->image((object)$attachment, 400, 400,[ 'class' => 'img-circle','alt' => 'User profile photo']);
                ?>
                <!-- badge -->
                <div class="rank-label-container">
                    <span class="label label-default rank-label"><?=  $this->request->session()->read('Auth.User.username') ?></span>
                </div>
            </div>
            <div class="profile-usertitle-job">
                Developer
            </div>
        </div>
        <!-- END SIDEBAR USER TITLE -->
        <!-- SIDEBAR BUTTONS -->
        <div class="profile-userbuttons">
            <a href="/" target="_blank" class="btn btn-success btn-sm">Visit site</a>
            <?= $this->Html->link('Your profile',['controller' => 'users','action' => 'myProfile'],['class' => 'btn btn-info btn-sm']); ?>
        </div>
        <!-- END SIDEBAR BUTTONS -->
        <!-- SIDEBAR MENU -->
        <div class="profile-usermenu">
            <ul class="nav">
                <li>
                    <a href="#">
                        <i class="fa fa-home"></i>
                        Overview </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-cogs"></i>
                        Config Settings </a>
                </li>
            </ul>
        </div>
        <!-- END MENU -->
    </div>
        <div class="sidebar-custom navbar-inverse sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <div class="side_logo text-center sidebar-background">
                </div>
                <ul class="nav" id="side-menu">
                    <li class="small-text-menu hide_el">Navigation</li>
                    <?= $this->Content->acutusSideMenu($sidebar_menu); ?>
                    <li>
                        <hr class="hr-sidebar-inverse margin-bottom-none">
                    </li>
                    <li class="bandwith-paddings">
                        <br>
                        <p class="text-uppercase hide_el bandwith-mini-header">Bandwidth <span class="pull-right bandwith-mini-percent"><strong>60%</strong></span>
                        </p>
                        <div class="progress hide_el bandwith-height-3">
                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%; ">
                                <span class="sr-only">60% Complete</span>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
</div>
