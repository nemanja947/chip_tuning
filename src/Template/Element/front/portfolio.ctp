<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header">Portfolio Heading</h2>
    </div>
    <div class="col-md-4 col-sm-6">
        <a href="portfolio-item.html">
            <?php echo $this->Format->image($user->attachments[0], 400, 400,['width' => 360, 'height' => 231, 'alt' => 'Ovo je slika']); ?>
        </a>
    </div>
    <div class="col-md-4 col-sm-6">
        <a href="portfolio-item.html">
            <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
        </a>
    </div>
    <div class="col-md-4 col-sm-6">
        <a href="portfolio-item.html">
            <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
        </a>
    </div>
    <div class="col-md-4 col-sm-6">
        <a href="portfolio-item.html">
            <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
        </a>
    </div>
    <div class="col-md-4 col-sm-6">
        <a href="portfolio-item.html">
            <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
        </a>
    </div>
    <div class="col-md-4 col-sm-6">
        <a href="portfolio-item.html">
            <img class="img-responsive img-portfolio img-hover" src="http://placehold.it/700x450" alt="">
        </a>
    </div>
</div>