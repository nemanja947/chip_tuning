<nav class="navbar navbar-default navbar-static-top hidden-xs">
    <div class="navbar-header">
        <div class="container-fluid">
            <div class="navbar-header hidden-lg hidden-md hidden-sm">
            </div>
        </div>
    </div>
    <!-- /.navbar-header -->
    <ul class="nav navbar-top-links navbar-left hidden-xs">
        <li>
            <a id="sidebar_toggle" href="#">
                <i class="fa fa-bars fa-fw fa-lg"></i>
            </a>
        </li>
    </ul>
    <ul class="nav navbar-top-links navbar-right">
        <!-- START Avatar -->
        <li>
            <a class="nav-brand dropdown dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <?php
                $attachment = $this->request->session()->read('Auth.User.attachments')[0];
                echo $this->Format->image((object)$attachment, 75, 75,[ 'class' => 'img-circle avatar-height-20','alt' => 'User profile photo']);
                ?>
                 <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li class="dropdown-header">Sign as <?= $this->request->session()->read('Auth.User.username'); ?></li>
                <li role="separator" class="divider"></li>
                <li>
                    <?= $this->Html->link("<i class=\"fa fa-user margin-right-5\"></i> Your Profile",['controller'=>'users','action'=>'myProfile'],['escape' => false]); ?>
                </li>
                <li><a href="#"><i class="fa fa-gear margin-right-5"></i> Settings</a>
                </li>
                <li><a href="#"><i class="fa fa-question-circle margin-right-5"></i> Help</a>
                </li>
                <li role="separator" class="divider"></li>
                <li>
                    <?= $this->Html->link("<i class=\"fa fa-sign-out margin-right-5\"></i> Sign Out",['controller'=>'Users','action'=>'logout'],['escape'=>false]); ?>
                </li>
            </ul>
        </li>
        <!--  Avatar END -->
        <!-- /.dropdown -->
        <!-- START Avatar -->
        <!--  Avatar END -->
    </ul>
    <ul class="nav navbar-top-links navbar-right">
        <!-- START Avatar -->
        <li>
            <a class="nav-brand dropdown dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                Tools
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
                <li><?= $this->Html->link("<i class=\"fa fa-question-circle margin-right-5\"></i> ".__('PHP info', true), ['controller' => 'users', 'action' => 'phpinfo'], ['title' => __('Server info', true),'escape'=>false]) ?></li>
                <li><a href="#"><i class="fa fa-gear margin-right-5"></i> Config</a></li>
                <li><?= $this->Html->link("<i class=\"fa fa-trash margin-right-5\"></i> ".__('Cleare cache', true), 'javascript:void(0)', ['title' => __('Delete all cache files', true),'onclick' => $this->Format->commit(['controller' => 'users', 'action' => 'clear','plugin'=>null]),'escape'=>false]) ?></li>
                <li><?= $this->Html->link("<i class=\"fa fa-question-circle margin-right-5\"></i> ".__('Database backup', true), ['controller' => 'users', 'action' => 'backup'], ['title' => __('Database backup', true),'escape'=>false]) ?></li>
            </ul>
        </li>
        <!--  Avatar END -->
        <!-- /.dropdown -->
        <!-- START Avatar -->
        <!--  Avatar END -->
    </ul>
</nav>