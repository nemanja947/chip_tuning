<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Acutus 1.1</title>
    <link rel="icon" type="image/png" href="../favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="../favicons/favicon-194x194.png" sizes="194x194">
    <link rel="icon" type="image/png" href="../favicons/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="../favicons/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="../favicons/favicon-16x16.png" sizes="16x16">
    <!--    <link rel="manifest" href="http://sumea.webkom.co/manifest.json">-->
    <meta name="msapplication-TileColor" content="#2e353b">
    <!--    <meta name="msapplication-TileImage" content="../../mstile-144x144.html">-->
    <meta name="theme-color" content="#ffffff">


    <?php
    echo $this->Html->css([
        'bootstrap.min',
        'styles-theme',
        'metisMenu.min',
        'font-awesome.min',
        //'chartist',
        //'chartist-settings-index',
        'build',
        'bootstrap-editable',
        'bootstrap-datepicker.min',
        'fontawesome-iconpicker.min',
        'switchery.min',
        'jquery-ui.min',
        'jquery.fancybox',
        'custom-tree'
    ]);

    echo $this->Html->script([
        'jquery.min',
        'ckeditor/ckeditor',
        'jquery-ui.min',
        'bootstrap.min',
        'bootstrap-datepicker.min',
        'fontawesome-iconpicker.min',
        'switchery.min',
        'jquery.mjs.nestedSortable',
    ]);

    if(isset($this->FrontendBridge)) {
        $this->FrontendBridge->init($frontendData);
        echo $this->FrontendBridge->run();
    }
    #Ucitavanje js fajlova za upload plugin
    echo $this->fetch('attachment');
    ?>


</head>
<body>
<div id="wrapper">
    <div class="clearfix">
        <div id="main_sidebar">
            <?= $this->element('acutus_sidebar'); ?>
        </div>
        <div id="main-content">
            <?= $this->element('acutus_topbar'); ?>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12" id="flashMessage">
                        <?= $this->Flash->render(); ?>
                    </div>
                </div>
                <div class="<?php echo $this->FrontendBridge->getMainContentClasses() ?>">
                    <?= $this->fetch('content'); ?>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
echo $this->Html->script([
    'metisMenu.min',
    'sumea-custom',
    //'jquery.fancybox.pack',
    'init'
]);
?>

<script>
    //    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    //            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    //        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    //    })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');
    //
    //    ga('create', 'UA-66904342-1', 'auto');
    //    ga('send', 'pageview');

</script>


<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
//        $('#data').dataTable();
//        $.fn.editable.defaults.mode = 'inline';

//      DATEPICKER FORMAT
        $(".datepicker").datepicker();

//      CKEDITOR sve sa klasom rte
        CKEDITOR.replaceAll( 'rte' );

//      FONT AWESOM PICKER
        $('.iconpicker-element').iconpicker();

        //$(".fancybox").fancybox();

    });


</script>
</body>
</html>