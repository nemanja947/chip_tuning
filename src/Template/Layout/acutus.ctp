<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Acutus 1.1</title>
    <meta name="msapplication-TileColor" content="#2e353b">
    <meta name="theme-color" content="#ffffff">
    <?php
    echo $this->Html->css([
        'bootstrap.min',
        'styles-theme',
        'metisMenu.min',
        'font-awesome.min',
        'build',
        'datatables-bootstrap3',
        'bootstrap-editable',
        'bootstrap-datepicker.min',
        'fontawesome-iconpicker.min',
        'switchery.min',
        'jquery-ui.min',
        'jquery.fancybox',
        'custom-tree',
        //'mdb.min',
        'sweet-alert.min'
    ]);

    echo $this->Html->script([
        'jquery.min',
        'jquery.fancybox',
        'ckeditor/ckeditor',
        'jquery-ui.min',
        'bootstrap.min',
        'bootstrap-datepicker.min',
        'fontawesome-iconpicker.min',
        'switchery.min',
        'jquery.mjs.nestedSortable',
    ]);

    if(isset($this->FrontendBridge)) {
        $this->FrontendBridge->init($frontendData);
        echo $this->FrontendBridge->run();
    }
    #Ucitavanje js fajlova za upload plugin
    echo $this->fetch('attachment');
    ?>
</head>
<body>
<!--<div class="loading">Loading&#8230;</div>-->
<div id="wrapper">
    <div class="clearfix">
        <div id="main_sidebar">
            <?= $this->element('acutus_sidebar'); ?>
        </div>
        <div id="main-content">
            <?= $this->element('acutus_topbar'); ?>
            <div id="page-wrapper">
                <div class="row">
                    <div class="col-md-12" id="flashMessage">
                        <?= $this->Flash->render(); ?>
                    </div>
                </div>
                <div class="<?php echo $this->FrontendBridge->getMainContentClasses() ?>">
                    <?= $this->fetch('content'); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $this->Html->script([
    'metisMenu.min',
    'jquery.dataTables.min',
    'datatables-bootstrap3',
    'sumea-custom',
    'sweet-alert',
    'init'
]);
?>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function () {
        $('#data').dataTable();
//        $.fn.editable.defaults.mode = 'inline';

//      DATEPICKER FORMAT
        $(".datepicker").datepicker();

//      CKEDITOR sve sa klasom rte
        CKEDITOR.replaceAll( 'rte' );

//      FONT AWESOM PICKER
        $('.iconpicker-element').iconpicker();

        $(".fancybox").fancybox();

    });
</script>
</body>
</html>