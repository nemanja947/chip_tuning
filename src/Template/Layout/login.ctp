<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <!--    <link rel="manifest" href="http://sumea.webkom.co/manifest.json">-->
    <meta name="msapplication-TileColor" content="#2e353b">
    <!--    <meta name="msapplication-TileImage" content="../../mstile-144x144.html">-->
    <meta name="theme-color" content="#ffffff">
    <?php
        echo $this->Html->css([
            'bootstrap.min',
            'font-awesome.min'
        ]);
    ?>
</head>
<body>
    <div id="wrapper" class="margin-top-15">
        <!-- Page Content -->
        <?= $this->fetch('content'); ?>
    </div>
</body>
</html>