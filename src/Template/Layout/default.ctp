<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Modern Business - Start Bootstrap Template</title>

    <?php
    echo $this->Html->css([
        'bootstrap.min',
        'modern-business.css',
        'font-awesome.min.css'
    ]);

    echo $this->Html->script([
        'jquery',
        'bootstrap.min',
    ]);
    ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php
    if(isset($this->FrontendBridge)) {
        $this->FrontendBridge->init($frontendData);
        echo $this->FrontendBridge->run();
    }
    #Ucitavanje js fajlova za upload plugin
    echo $this->fetch('attachment');
    ?>


</head>
<body>
<!--    Navigation-->
<?php echo $this->element('front/menu'); ?>

<!-- Header Carousel -->
<?php echo $this->element('front/slider'); ?>

<div class="container">

    <!-- Marketing Icons Section -->
    <?php echo $this->element('front/icons-section'); ?>

    <!-- Portfolio Section -->
    <?php echo $this->element('front/portfolio'); ?>


    <?= $this->fetch('content'); ?>
    <!-- Footer -->
    <?php echo $this->element('front/footer'); ?>

</div>

<!-- Script to Activate the Carousel -->
<script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
</script>

</body>
</html>