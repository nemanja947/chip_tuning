<div class="page-header no-padding">
    <ul class="breadcrumb" id="page-breadcrumb">
        <li><?php echo $this->Html->link(__('Pages Type'), array('action' => 'index')); ?></li>
                <li class="active"><?php echo $this->Html->link(__('Edit'), array('action' => 'edit')); ?></li>
                
    </ul>
</div>

<?= $this->Flash->render(); ?>
<div class="row">
    <div class="col-sm-12">
        <?= $this->Form->create($pagesType) ?>
        <div class="panel-body">
            <?php
                echo $this->Form->input('title');
                echo $this->Form->input('is_active', ['type'=>'checkbox', 'hiddenField' => true]);
            ?>
        </div>
        <div class="panel-footer text-right">
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>

        <div class="panel-heading">
            <span class='panel-title'><?php echo __('Actions'); ?></span>
        </div>
        <div class="panel-body buttons-with-margins"  >
            <?php echo $this->Html->link(__('List Pages Type'), array('action' => 'index'), array('class' => 'btn btn-primary')) ?>
                            <?= $this->Form->postLink(
            __('Delete'),
            ['action' => 'delete', $pagesType->id],
            [
            'confirm' => __('Are you sure you want to delete # {0}?', $pagesType->id),
            'class' => 'btn btn-danger'
            ])
            ?>
                    </div>
    </div>
</div>

