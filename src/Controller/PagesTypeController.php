<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PagesType Controller
 *
 * @property \App\Model\Table\PagesTypeTable $PagesType
 */
class PagesTypeController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $pagesType = $this->paginate($this->PagesType);

        $this->set(compact('pagesType'));
        $this->set('_serialize', ['pagesType']);
    }

    /**
     * View method
     *
     * @param string|null $id Pages Type id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pagesType = $this->PagesType->get($id, [
            'contain' => []
        ]);

        $this->set('pagesType', $pagesType);
        $this->set('_serialize', ['pagesType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pagesType = $this->PagesType->newEntity();
        if ($this->request->is('post')) {
            $pagesType = $this->PagesType->patchEntity($pagesType, $this->request->data);
            if ($this->PagesType->save($pagesType)) {
                $this->Flash->success(__('The pages type has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pages type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pagesType'));
        $this->set('_serialize', ['pagesType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pages Type id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pagesType = $this->PagesType->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pagesType = $this->PagesType->patchEntity($pagesType, $this->request->data);
            if ($this->PagesType->save($pagesType)) {
                $this->Flash->success(__('The pages type has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The pages type could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('pagesType'));
        $this->set('_serialize', ['pagesType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Pages Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pagesType = $this->PagesType->get($id);
        if ($this->PagesType->delete($pagesType)) {
            $this->Flash->success(__('The pages type has been deleted.'));
        } else {
            $this->Flash->error(__('The pages type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
