<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Cache\Cache;
use Cake\Core\Configure;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    var $service_actions=[];
    var $front=[];
    public $helpers=['Url'];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Groups']
        ];
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    public function dashboard(){

    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Groups', 'Attachments']
        ]);

//        $imageOperations = [
//            'thumbnail' => [
//                'height' => 50,
//                'width' => 50
//            ],
//        ];
//
//        $this->Users->processImage(
//            WWW_ROOT . 'attachments' . DS . $user->attachments[0]->filepath,
//            WWW_ROOT . 'thumbs' . DS . 'thumb-' . $user->attachments[0]->filename,
//            [],
//            $imageOperations
//        );
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $groups = $this->Users->Groups->find('list', ['limit' => 200]);
        $this->set(compact('user', 'groups'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Attachments']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $groups = $this->Users->Groups->find('list', ['limit' => 200]);
        $this->set(compact('user', 'groups'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function myProfile(){

        $id = $this->request->session()->read('Auth.User.id');
        $user = $this->Users->get($id, [
            'contain' => ['Attachments']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);

            # Only change pass if this fields is empty
            if($this->request->data['old_password'] != '' || $this->request->data['password'] != '') {
                $user = $this->Users->patchEntity($user, [
                    'old_password' => $this->request->data['old_password'],
                    'password' => $this->request->data['password'],
                    'confirm_password' => $this->request->data['confirm_password'],
                ],
                    ['validate' => 'password']);
            }else{
                unset($user->password);
                unset($user->old_password);
                unset($user->confirm_password);
            }
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect(['action' => 'myProfile']);
            } else {
                $this->Flash->error(__('The user could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    public function login(){
        $this->viewBuilder()->layout('login');

        if($this->request->is('post')){
            $user=$this->Auth->identify();
            if($user){
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl(['controller'=>'Users','action'=>'dashboard']));
            }

            $this->Flash->error("Your username or password is incorrect!");
        }
    }

    public function logout(){
        $this->Flash->success('You are now logged out!');
        return $this->redirect($this->Auth->logout());
    }

    #Mozda premestim na neko drugo mesto
    /**
     * @param bool $frame
     */
    public function phpinfo($frame = false) {
        if ($frame) {
            phpinfo();
            die();
        }
        //$this->set('page', array('Menu' => array('title' => __('PHP Info', true))));
    }

    public function clear() {
        $cache = Cache::clear(true) && $this->cache_clear() ?
            __('Cache succesfully cleared.', true) :
            __('There was an error deleting the cache files!', true);
//        $thumbs = $this->unlinkSubTree(WWW_ROOT . 'thumbs') ?
//            __('Thumbs succesfully deleted.', true) :
//            __('There was an error deleting thumbs!', true);
        die((Configure::read('acutus.cache') ? $cache : __('Cache engine not enabled.', true)) . '<br />' . $thumbs);
    }

    # clear ALL cache dir #
    public function cache_clear() {
        echo '<button data-dismiss="alert" class="close" type="button">×</button><h3>CLEAR ALL CACHE</h3>';
        $cacheBase = '../tmp/cache/';
        $model_cache = glob($cacheBase . '{persistent,models,data,views,}/*', GLOB_BRACE); //dont touch "," before bracket close!!!
        echo '<p>';
        if (!empty($model_cache)) {
            foreach ($model_cache as $file) {
                if (is_file($file)) {
                    unlink($file);
                    echo 'removed ' . $file . '<br>';
                }
            }
        }
        echo "</p>";
        die;
    }

    # function for db_backup

    public function backup(){
        debug(__DIR__ ); die;
//        set_time_limit(0);
//        ignore_user_abort(TRUE);
//        require __DIR__ . '/../Vendor/MySQL-dump/MySQLDump.php';
//
//        $conn = new DATABASE_CONFIG;
//        $dump = new \app\core\backup\MySQLDump(new mysqli($conn->default['host'], $conn->default['login'], $conn->default['password'], $conn->default['database']));
//
//        // store backups to directory '/backups' and keep at most 30 backups
//        require __DIR__ . '/../Vendor/MySQL-dump/DatabaseBackup.php';
//        $backup = new \app\core\backup\DatabaseBackup($dump, __DIR__ . '/../../backups', 30);
//        $backup->backupDatabase();
//
//        ini_set('zlib.output_compression', TRUE);
//        header('Content-Type: application/x-gzip');
//        header('Content-Disposition: attachment; filename="database ' . date('Y-m-d H-i') . '.sql.gz"');
//        header('Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT');
//        header('Cache-Control: no-cache');
//        header('Connection: close');

        $dump->write();
    }
}
