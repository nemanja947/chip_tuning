<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;
use Cake\View\View;
use App\View\Helper\ContentHelper;
use Cake\View\Helper;

/**
 * CmsMenu Controller
 *
 * @property \App\Model\Table\CmsMenuTable $CmsMenu
 */
class CmsMenuController extends AppController
{

    var $service_actions=['proba'];

    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $cmsMenu = $this->CmsMenu->find('all')->contain('ParentCmsMenu');

        $this->set(compact('cmsMenu'));
        $this->set('_serialize', ['cmsMenu']);

        $menu=$this->CmsMenu->find('threaded',[
            'keyField' => $this->CmsMenu->primaryKey(),
            'parentField' => 'parent_id',
            'order'=>'CmsMenu.lft ASC'
        ]);
        $this->set('menu',$menu);
    }

    /**
     * Refresh Acutus side menu
     */
    public function refreshMenu(){
        $menu = $this->getSideMenu();

        $content = new ContentHelper(new View());
        $fresh_menu = $content->acutusSideMenu($menu);
        $data = [
            'html' => $fresh_menu
        ];
        $this->set('data', $data);
        $this->set('_serialize', ['data']);
    }

    /**
     * Reorganizacija menija
     */
    public function reorganize(){

        Router::extensions('json', 'xml');

        if ($this->request->is('ajax')) {
            $request_data = $this->request->data;

            $cmsMenu = TableRegistry::get('CmsMenu');

            $newMenu = $cmsMenu->newEntity();
            foreach ($request_data as $data) {
                $newMenu->id = $data['id'];
                $newMenu->parent_id = $data['parent_id'];
                $newMenu->lft = $data['lft'];
                $newMenu->rght = $data['rght'];

                $this->CmsMenu->save($newMenu);
            }
            #Refresh side menu
            $this->refreshMenu();

        }
    }

    /**
     * Change menu title via ajax
     */
    public function changeTitle(){
        Router::extensions('json', 'xml');
        if ($this->request->is('ajax')) {
            $request_data = $this->request->data;

            $cmsMenu = TableRegistry::get('CmsMenu');
            $menu = $cmsMenu->get($request_data['id']);
            $cmsMenu->patchEntity($menu, $request_data);
            $cmsMenu->save($menu);

            #Refresh side menu
            $this->refreshMenu();
        }
    }

    /**
     * View method
     *
     * @param string|null $id Cms Menu id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $cmsMenu = $this->CmsMenu->get($id, [
            'contain' => ['ParentCmsMenu', 'ChildCmsMenu']
        ]);

        $this->set('cmsMenu', $cmsMenu);
        $this->set('_serialize', ['cmsMenu']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cmsMenu = $this->CmsMenu->newEntity();
        if ($this->request->is('post')) {
            $cmsMenu = $this->CmsMenu->patchEntity($cmsMenu, $this->request->data);
            if ($this->CmsMenu->save($cmsMenu)) {
                $this->Flash->success(__('The cms menu has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cms menu could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('cmsMenu'));
        $this->set('_serialize', ['cmsMenu']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Cms Menu id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $cmsMenu = $this->CmsMenu->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $cmsMenu = $this->CmsMenu->patchEntity($cmsMenu, $this->request->data);
            if ($this->CmsMenu->save($cmsMenu)) {
                $this->Flash->success(__('The cms menu has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cms menu could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('cmsMenu'));
        $this->set('_serialize', ['cmsMenu']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Cms Menu id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $cmsMenu = $this->CmsMenu->get($id);
        if ($this->CmsMenu->delete($cmsMenu)) {
            $this->Flash->success(__('The cms menu has been deleted.'));
        } else {
            $this->Flash->error(__('The cms menu could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function beforeFilter(Event $event)
    {

        $parentCmsMenu = $this->CmsMenu->find('treeList', [
        'keyPath' => 'id',
        'valuePath' => 'title',
        'spacer' => '_'
    ]);
        $this->set('resources',$this->getResources('acutus_actions'));
        $this->set('parentCmsMenu',$parentCmsMenu);
        return parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }
}
