<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

/**
 * Pages Controller
 *
 * @property \App\Model\Table\PagesTable $Pages
 */
class PagesController extends AppController
{
    var $front=['details'];

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $pages = $this->Pages->find('all')->contain('ParentPages');

        $this->set(compact('pages'));
        $this->set('_serialize', ['pages']);

        $menu=$this->Pages->find('threaded',[
            'keyField' => $this->Pages->primaryKey(),
            'parentField' => 'parent_id',
            'order'=>'Pages.lft ASC'
        ]);
        $this->set('menu',$menu);
    }

    /**
     * Reorganizacija menija
     */
    public function reorganize(){

        Router::extensions('json', 'xml');

        if ($this->request->is('ajax')) {
            $request_data = $this->request->data;

            $pages = TableRegistry::get('Pages');
            $newMenu = $pages->newEntity();
            foreach ($request_data as $data) {
                $newMenu->id = $data['id'];
                $newMenu->parent_id = $data['parent_id'];
                $newMenu->lft = $data['lft'];
                $newMenu->rght = $data['rght'];

                $this->Pages->save($newMenu);
            }
            $data = [
                'message' => 'radi sve!'
            ];

            $this->set('data', $data);
            $this->set('_serialize', ['data']);
        }
    }

    /**
     * Change menu title via ajax
     */
    public function changeTitle(){
        Router::extensions('json', 'xml');
        if ($this->request->is('ajax')) {
            $request_data = $this->request->data;

            $pages = TableRegistry::get('Pages');
            $menu = $pages->get($request_data['id']);
            $pages->patchEntity($menu, $request_data);
            $pages->save($menu);
        }
    }

    /**
     * View method
     *
     * @param string|null $id Page id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $page = $this->Pages->get($id, [
            'contain' => ['ParentPages', 'ChildPages']
        ]);

        $this->set('page', $page);
        $this->set('_serialize', ['page']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $page = $this->Pages->newEntity();
        if ($this->request->is('post')) {
            $page = $this->Pages->patchEntity($page, $this->request->data);
            if ($this->Pages->save($page)) {
                $this->Flash->success(__('The page has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The page could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('page'));
        $this->set('_serialize', ['page']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Page id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $page = $this->Pages->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $page = $this->Pages->patchEntity($page, $this->request->data);
            if ($this->Pages->save($page)) {
                $this->Flash->success(__('The page has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The page could not be saved. Please, try again.'));
            }
        }

        $this->set(compact('page'));
        $this->set('_serialize', ['page']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Page id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $page = $this->Pages->get($id);
        if ($this->Pages->delete($page)) {
            $this->Flash->success(__('The page has been deleted.'));
        } else {
            $this->Flash->error(__('The page could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function display()
    {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }

    public function details($id = null){
        if(isset($id)){

            $page = $this->Pages->get($id);
            if( !empty($page)){
                $this->set('page', $page);
            }
        }
    }

    public function beforeFilter(Event $event)
    {
        $parentPages = $this->Pages->find('treeList', [
            'keyPath' => 'id',
            'valuePath' => 'title',
            'spacer' => '_'
        ]);

        $types = $this->Pages->Type->find('list', [
            'keyField' => 'id',
            'valueField' => 'title'
        ]);

        $this->set('resources',$this->getResources('service_actions'));
        $this->set(compact('parentPages', 'types'));
        return parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }
}
