<?php
namespace App\Controller;

use Cake\ORM\TableRegistry;

class HomeController extends AppController
{
    public function home(){

        #Ovo je test za thumb
        $this->User = TableRegistry::get('Users');

        $user = $this->User->get(1, [
            'contain' => ['Attachments']
        ]);

        $this->set('user', $user);
    }
}
