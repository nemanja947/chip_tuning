<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use App\View\Helper\FormatHelper;
use Cake\View\View;
use Cake\Core\Configure;
use Cake\I18n\I18n;


/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    var $service_actions = []; # dinamic service for pages
    var $acutus_actions = ['index','edit','add']; # backend services
    var $front = ['home']; # front services
    public $helpers = ['Format','Form' => ['templates' => 'acutus_form',],'Html','Content','Attachments.Attachments','FrontendBridge' => ['className' => 'FrontendBridge.FrontendBridge']];

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('FrontendBridge.FrontendBridge');
        $this->loadComponent('Flash');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Auth',[
            'authenticate'=>[
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ],
                    'finder' => 'auth'
                ]
            ],
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ]
            ]);


    }
    
    /**
     * get Controller list
     * @return array
     */
    public function getControllers(){
        $files = scandir('../src/Controller/');
        $results = [];
        $ignoreList = [
            '.',
            '..',
            'Component',
            'AppController.php',
        ];
        foreach($files as $file){
            if(!in_array($file, $ignoreList)) {
                $controller = explode('.', $file)[0];
                array_push($results, str_replace('Controller', '', $controller));
            }
        }
        return $results;
    }

    /**
     * get all actions
     * @param $controllerName
     * @return array
     */
    public function getActions($controllerName,$actions_type) {
        $className = 'App\\Controller\\'.$controllerName.'Controller';
        $vars = get_class_vars($className);
        //$actions = $vars['acutus_actions'];
        if(isset($actions_type)){
            $actions=$vars[$actions_type];
        }
        $results=[];
        if(!empty($actions)) {
            foreach ($actions as $action) {
                    $link = $controllerName . "/" . $action;
                    $results[$link] = $link;
            }
        }
        return $results;
    }

    /**
     * @return array
     */
    public function getResources($actions_type = null){
        $controllers = $this->getControllers();
        $resources = [];
        foreach($controllers as $controller){
            $actions = $this->getActions($controller,$actions_type);
            $resources[$controller]=$actions;
        }
        return $resources;
    }

    /**
     * Side menu for backend
     */
    public function getSideMenu(){

        $this->Menu = TableRegistry::get('CmsMenu');
        $menu=$this->Menu->find('threaded',[
            'keyField' => $this->Menu->primaryKey(),
            'parentField' => 'parent_id',
            'order'=>'CmsMenu.lft ASC'
            ]);

        return $menu;
    }

    /**
     * Menu for front
     */
    public function getFrontMenu(){
        $this->Pages = TableRegistry::get('Pages');
        $menu=$this->Pages->find('threaded',[
            'keyField' => $this->Pages->primaryKey(),
            'parentField' => 'parent_id',
            'conditions' => ['Pages.is_visible' => true],
            'order'=>'Pages.lft ASC'
        ]);
        $this->set('front_menu',$menu);
    }

    #Funckija za tinyint polja
    /**
     * @param null $id
     * @param null $field
     * @param null $sufix
     */
    function switchBool($id = null, $field = null, $sufix = null){

        $this->render=false;
        $field = ltrim($original = $field, '!');
        $model = $this->{$this->modelClass};


        $type=$model->schema()->columnType($field);
        if (!$model->hasField($field) || $type != 'boolean') {
            die(__('Error', true) . '!');
        }

        $data=$model->get($id);
        $data[$field]=!$data[$field];
        $model->save($data);

        #Ispis rezultata
        $format=new FormatHelper(new View());
        die($format->toggle($data,$field,'switchBool', $sufix));
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * Get all Acutus languages and set to Views
     */
    public function getLanguages(){
        $this->languages = TableRegistry::get('Languages');
        $languages = $this->languages->find('all')->where(['Languages.is_active' => true])->contain('Attachments');
        //debug($languages->toArray()); die;
        $this->set('cms_languages',$languages->toArray());
    }

    public function beforeFilter(Event $event){
        # set translation
        I18n::locale('en');
        $this->action=$this->request->action;
        $this->controller=$this->request->controller;
        #Custom config
//        Configure::write('myconf', ['aaaa']);
//        debug(Configure::read('BASE_URL'));die;

        #This is default scope session
        Configure::write('scope', 'front');

        #Remove login for front actions
        $this->Auth->allow($this->front);

        # set theme if in backend
        if (!in_array($this->action, $this->front)) {
            $this->getLanguages();
            Configure::write('App.imageBaseUrl', '/acutus_theme/img/');
            Configure::write('App.cssBaseUrl', '/acutus_theme/css/');
            Configure::write('App.jsBaseUrl', '/acutus_theme/js/');
            $this->viewBuilder()->layout('acutus');
            Configure::write('scope', 'acutus');
        }

        #Setovanje vrednosti za backend
        $scope=Configure::read('scope');
        if ($scope == 'acutus' ) {
            $menu = $this->getSideMenu();
            $this->set('sidebar_menu', $menu);
        }
        #Setovanje vrednosti za front
        elseif ($scope == 'front'){
            #setovanje front menija
            $this->getFrontMenu();
        }

        return parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }
}
