<?php
/**
 * Created by PhpStorm.
 * User: Nemanja94
 * Date: 9/3/2016
 * Time: 3:01 PM
 */

namespace App\View\Helper;
use Cake\View\Helper;
use Cake\ORM\TableRegistry;

/**
 * Class ContentHelper
 * @package App\View\Helper
 */
class ContentHelper extends Helper
{
    public $helpers = ['Html','Form'];

    /**
     * @param null $sidebar_menu
     * @return string
     */
    public function acutusSideMenu($sidebar_menu = null){
        $text="";
         if(!empty($sidebar_menu)){
            foreach ($sidebar_menu as $menu){
                $text.= "<li class='current-parent current'>";
                if(!empty($menu->children)) {
                    $text.= "<a href=\"javascript:void(0)\" data-toggle=\"side_tooltip_offset\" data-placement=\"right\" title=\"{$menu->title}\"><span class=\"fa arrow\"></span><i class=\"fa {$menu->icon}\"></i> <span class=\"menu_title\">{$menu->title}</span></a>";
//                    $text .= "<ul class=\"nav nav-second-level\">";
//                    foreach ($menu->children as $sub_menu) {
//                        $text .= "<li>";
//                        $text .= $this->Html->link($sub_menu->title, "/" . $sub_menu->controller);
//                        $text .= "</li>";
//                    }
//                    $text.= "</ul>";
                    $text .= "<ul class=\"nav nav-second-level\">";
                    $text.=$this->acutusSideMenu($menu->children);
                    $text.= "</ul>";
                }
                else{
                    $text.= $this->Html->link("<i class=\"fa {$menu->icon}\"></i> <span class=\"menu_title\">{$menu->title}</span>","/".$menu->controller,['class'=>'current-parent current','data-toggle'=>'side_tooltip','data-placement'=>'right','title'=>$menu->title,'escape'=>false]);
                }
                $text.= "</li>";
            }
        }
        return $text;
    }

    /**
     * @param null $front_menu
     * @return string
     */
    public function generateFrontMenu($front_menu = null){
        $text ="";
        if(!empty($front_menu)){
            foreach ($front_menu as $menu) {
                $link = $this->getLinkType( $menu );
                $target = $menu->new_window ? '_blank' : '_self';

               // debug($menu); die;
                $text .= "<li>";
                if(!empty($menu->children)) {
                    $text .= "<a href=\"{$link}\" data-toggle=\"dropdown\" target=\"{$target}\"  title=\"{$menu->menu_subtitle}\">{$menu->menu_subtitle} <b class=\"caret\"></b></a>";
                    $text .= "<ul class=\"dropdown-menu\">";
                    $text .=$this->generateFrontMenu($menu->children);
                    $text .= "</ul>";
                }
                else{
                    $text .= $this->Html->link($menu->menu_subtitle,$link,['target' => $target,'escape'=>false]);
                }
                $text .= "</li>";
            }
        }
        return $text;
    }

    function getLinkType( $menu ){
        switch ($menu->type_id){
            case 1:
                $link = ['controller' => 'pages' , 'action' => 'details' , $menu->id];
                break;
            case 2:
                $link = '/' . $menu->service;
                break;
            case 3:
                $link = isset($menu->anchor) && $menu->anchor != '' ?  $menu->anchor : "#";
                break;
            default:
                $link = "#";
        }

        return $link;
    }

    #Generisanje tree  structure za meni
    /**
     * @param $menu
     * @param int $level
     * @return string
     */
    function MyTree($menu,$level=0){
        if($level == 0) {
            $text = "<ol class=\"sortable\">";
        }else{
            $text = "<ol>";
        }

        foreach ($menu as $value){
            $actions = $this->Html->link('<i class="menu-icon fa  fa-pencil"></i>', ['action' => 'edit', $value->id],['title' => __('Edit', true), 'escape' => false, 'class' => 'btn btn-primary']);
            $actions .= $this->Form->postLink('<i class="menu-icon fa fa-trash-o"></i>', ['action' => 'delete', $value->id], ['class' => 'btn btn-danger', 'title' => __('Delete', true), 'escape' => false,'confirm' => __('Are you sure you want to delete # {0}?',$value->id)]);
            if(!empty($value->children)) {
                $text .="<li id=\"menuItem_{$value->id}\">
                <div>
                <i class=\"disclose pull-left fa fa-minus\" aria-hidden=\"true\"></i>
                <span class='btn btn-success btn-sm change-title' data-id={$value->id}>{$value->title}</span>
                <span class='pull-right'> {$actions} </span>
                </div>";
                $text .=$this->MyTree($value->children,++$level);
            }else{
                $text .= "<li id=\"menuItem_{$value->id}\" >
                <div>
                <span class='btn btn-success btn-sm change-title' data-id={$value->id}>{$value->title}</span>
                <span class='pull-right'> {$actions} </span>
                </div></li>";
            }
        }
        $text.="</ol>";

        return $text;
    }

    /**
     * Function for generate translations input fields
     * @param $table
     * @param $languages
     */
    public function translations($table, $languages){
        $this->table = TableRegistry::get($table);
        $fields = $this->table->behaviors()->get('Translate')->config('fields');

            echo "<div class=\"tabbable-line\">";
            echo "<ul class='nav nav-tabs'>";

            $number_lang = count($languages);
            $i = 0;
            foreach ($languages as $language){
                if($i < $number_lang) {
                    $class = ($i == 0) ? "class='active'" : "";
                    echo "<li {$class}><a data-toggle=\"tab\" href=\"#lang{$i}\">{$language->title}</a></li>";
                    $i++;
                }
            }
            echo "</ul></div>";

            echo "<div class=\"tab-content\">";
            $i = 0;
            foreach ($languages as $language){
                $class = ($i == 0) ? "active" : "";
                echo "<div id=\"lang{$i}\" class=\"tab-pane fade in {$class}\">";
                    foreach ($fields as $field){
//                        if( $i == 0)
//                            echo $this->Form->input($field);
//                        else
                            echo $this->Form->input('_translations.'. $language->locale . "." . $field);
                    }
                    $i++;
                echo "</div>";
            }
            echo "</div>";


    }

}