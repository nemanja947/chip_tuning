<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\Core\Configure;
/**
 * Format helper
 */

class FormatHelper extends Helper
{

    public $helpers = ['Html', 'Url'];

    #Funkcija za tinyint polja
    /**
     * @param $object
     * @param $field
     * @param null $controller
     * @param string $function
     * @param string $sufix
     * @param null $html
     * @return null|string
     */
    function toggle($object, $field, $function = 'switchBool', $sufix = '', $html = null)
    {


        $controller = $this->request->params['controller'];

        if (!method_exists('App\Controller\AppController', $function)) {
            return __('Error!', true);
        }

        $src = $field{0} == '!'
            ? (int)!$object[ltrim($field, '!')]
            : (int)$object[$field];


        $icon = $src == 1 ? 'fa-check text-success' : 'fa-ban text-danger';
        return
            $this->Html->link('<i class="menu-icon fa ' . $icon . '"></i>',
                'javascript:void(0);',
                array(
                    'onclick' => "$(this).parent().html('<i class=\'fa fa-spinner fa-spin\'></i>').load('/{$controller}/{$function}/{$object['id']}/{$field}/{$sufix}', function() { if(typeof(afterToggle) == 'function') afterToggle(this) })",
                    'escape' => false
                )
            );
    }

    #Prikaz obrisanih fajlova iz kesa
    function commit($url, $confirm = false)
    {
        if (is_string($url) && $confirm === null) {
            return 'cake.flash(\'' . $url . '\', \'url(' . $this->Url->image('/img/loading_flash.gif') . ')\')';
        }
        $action = 'cake.flash(\'' . __('Please wait...', true) . '\', \'url(' . $this->Url->image('/img/loading_flash.gif') . ')\'); $.get(\'' . $this->Url->build($url, true) . '\', null, function(response){cake.flash(response)});';
        return $confirm ?
            "if(confirm('{$confirm}')) { {$action} }" :
            $action;
    }

    /**
     *** Function for image thumbs
     * @param $attachment
     * @param null $thumb_width
     * @param null $thumb_height
     * @param array $options
     * @param string $no_image
     * @return mixed
     */
    public function image($attachment, $thumb_width = null, $thumb_height = null, $options = Array(), $no_image = 'no-image.png')
    {
        if (!empty($attachment)) {
            $thumbsDir = Configure::read('Attachments.thumbsDir');
            $image = $attachment->model . DS . $attachment->foreign_key . DS . $thumb_width . 'x' . $thumb_height . '-' . $attachment->filename;
            $file_path = $thumbsDir . $image;

            $thumbsUrl = Configure::read('Attachments.thumbsUrl');
            if (file_exists($file_path)) {
                return $this->Html->image($thumbsUrl . $attachment->model . '/' . $attachment->foreign_key . '/' . $thumb_width . 'x' . $thumb_height . '-' . $attachment->filename, $options);
            }
            else{
                #Ovde vratiti original sliku
                return $this->Html->image('/attachments/' . $attachment->model . '/' . $attachment->foreign_key . '/' . $attachment->filename , $options);
            }
        } else {
            return $this->Html->image($no_image, $options);
        }
    }
}