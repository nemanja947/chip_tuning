<%
/**
* CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
* Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
*
* Licensed under The MIT License
* For full copyright and license information, please see the LICENSE.txt
* Redistributions of files must retain the above copyright notice.
*
* @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
* @link          http://cakephp.org CakePHP(tm) Project
* @since         0.1.0
* @license       http://www.opensource.org/licenses/mit-license.php MIT License
*/
use Cake\Utility\Inflector;

$fields = collection($fields)
->filter(function($field) use ($schema) {
return $schema->columnType($field) !== 'binary';
});

if (isset($modelObject) && $modelObject->behaviors()->has('Tree')) {
$fields = $fields->reject(function ($field) {
return $field === 'lft' || $field === 'rght';
});
}
%>
<div class="page-header no-padding">
    <ul class="breadcrumb" id="page-breadcrumb">
        <li><?php echo $this->Html->link(__('<%= $pluralHumanName %>'), array('action' => 'index')); ?></li>
        <% if (strpos($action, 'add') === false): %>
        <li class="active"><?php echo $this->Html->link(__('Edit'), array('action' => 'edit')); ?></li>
        <% endif;%>
        <% if (strpos($action, 'edit') === false): %>
        <li class="active"><?php echo $this->Html->link(__('Add'), array('action' => 'add')); ?></li>
        <% endif;%>

    </ul>
</div>

<?= $this->Flash->render(); ?>
<div class="row">
    <div class="col-sm-12">
        <?= $this->Form->create($<%= $singularVar %>) ?>
        <div class="panel-body">
            <?php
            <%
            foreach ($fields as $field) {
                if (in_array($field, $primaryKey)) {
                    continue;
                }
                if (isset($keyFields[$field])) {
                    $fieldData = $schema->column($field);
                    if (!empty($fieldData['null'])) {
                        %>
                        echo $this->Form->input('<%= $field %>', ['options' => $<%= $keyFields[$field] %>, 'empty' => true]);
                        <%
                    } else {
                        %>
                        echo $this->Form->input('<%= $field %>', ['options' => $<%= $keyFields[$field] %>]);
                        <%
                    }
                    continue;
                }
                if (!in_array($field, ['created', 'modified', 'updated'])) {
                    $fieldData = $schema->column($field);
                    if (in_array($fieldData['type'], ['date', 'datetime', 'time']) && (empty($fieldData['null']))) {
                        %>
                        echo $this->Form->input('<%= $field %>', ['type'=>'text','class'=>'form-control datepicker']);
                        <%
                    } elseif (in_array($fieldData['type'], ['boolean']) && (empty($fieldData['null']))){
                        %>
                        echo $this->Form->input('<%= $field %>', ['type'=>'checkbox', 'hiddenField' => true]);
                        <%
                    } elseif (in_array($fieldData['type'], ['text']) && (empty($fieldData['null']))){
                        %>
                        echo $this->Form->input('<%= $field %>', ['type'=>'textarea' , 'class' => 'rte']);
                        <%
                    }
                    else {
                        %>
                        echo $this->Form->input('<%= $field %>');
                        <%
                    }
                }
            }
            if (!empty($associations['BelongsToMany'])) {
                foreach ($associations['BelongsToMany'] as $assocName => $assocData) {
                    %>
                    echo $this->Form->input('<%= $assocData['property'] %>._ids', ['options' => $<%= $assocData['variable'] %>]);
<%
            }
            }
            %>
            ?>
        </div>
        <div class="panel-footer text-right">
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>

        <div class="panel-heading">
            <span class='panel-title'><?php echo __('Actions'); ?></span>
        </div>
        <div class="panel-body buttons-with-margins"  >
            <?php echo $this->Html->link(__('List <%= $pluralHumanName %>'), array('action' => 'index'), array('class' => 'btn btn-primary')) ?>
                <% if (strpos($action, 'add') === false): %>
            <?= $this->Form->postLink(
            __('Delete'),
            ['action' => 'delete', $<%= $singularVar %>-><%= $primaryKey[0] %>],
            [
            'confirm' => __('Are you sure you want to delete # {0}?', $<%= $singularVar %>-><%= $primaryKey[0] %>),
            'class' => 'btn btn-danger'
            ])
            ?>
            <% endif; %>
        </div>
    </div>
</div>

